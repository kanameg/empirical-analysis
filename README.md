# Dev Containerを使った開発環境

Dev Containersを使ったPythonとJupyterLabの開発環境です。
Pythonのバージョン3.10.13と以下のライブラリがインストールされています。

- category-encoders
- folium
- jeraconv
- jupyterlab
- kaggle
- lightgbm
- matplotlib
- numpy
- openpyxl
- PyYAML
- pandas
- seaborn
- scipy
- scikit-learn
- signate
- statsmodels


## ディレクトリ構成

```
.
├── .devcontainer
│   ├── devcontainer.json       # Dev Containerの設定ファイル
│   ├── docker-compose.yml      # Docker Composeの設定ファイル
│   └── python                  # pythonコンテナ
│       ├── Dockerfile          # pythonコンテナのDockerfile
│       └── requirements.txt    # pythonコンテナのライブラリリスト
├── .gitignore
├── README.md                   # このファイル
└── workspace                   # コンテナにマウントされるWorkSpace
    ├── .vscode                 # プロジェクトのvscode設定
    │   ├── launch.json        # デバック実行設定
    │   └── setting.jsontxt    # WorkSpace用の設定ファイル
    ├── config                  # 設定ファイル置き場(空)
    ├── data                    # データ置き場
    ├── log                     # ログ置き場
    ├── model                   # 学習モデル置き場
    └── src                     # プロジェクトソース
```

### 利用方法
1. このリポジトリを、新しいプロジェクト名の`project name`にクローンする
```bash
git clone https://gitlab.com/kanameg/dev-container-jupyter.git <project_name>
```
2. `project name`ディレクトリに移動する
```bash
cd <project_name>
```
3. `project name`ディレクトリのgit管理ファイルを削除する
```bash
rm -rf .git
```
4. `project name`ディレクトリを新にgit管理する
```bash
git init --initial-branch main
```

---
### 参考ページ

コンテナ起動時にコンテナ名がぶつかる問題
[【Docker Compose】container_name ではなくて COMPOSE_PROJECT_NAME 変数を使うと良さそう](https://qiita.com/okashoi/items/1ddf3724ad5c166e417b)
